/**
 * Created by JJOMJAMM on 4/7/2559.
 */
var connection = require("../../config/database");
var login = {};

login.renderPage = function (req,res,next) {
  res.send("loginsend");
};

login.verifyLogin =  function (req,res,next) {
    var email = req.body.email;
    var passwords = req.body.password;
    var sql = "select * from mydb.tb_login where email = ? and passwords = ?" ;
    var sql_data = [email,passwords];
    var message = "ไม่สามารถเข้าสู่ระบบได้";
    connection.query(sql,sql_data,function (err, rows, fields) {

        if (!err){
            if(rows && rows.length === 1) {
                var email = rows[0].email;
                var status = rows[0].status;
                if(status) {
                    req.session.authentication  = 'user';
                } else {
                    req.session.authentication  = 'administrator';
                }
                req.session.email = email;
                res.send({
                    email : email,
                    isAdministrator : (status)? false : true
                });
            } else {
                res.send({message: message});
            }
        }else {
            res.send({message: 'เกิดข้อผิดพลาดจากระบบ'});
        }
    });

};

login.logout = function (req,res,next) {
    req.session.authentication = undefined;
    res.redirect('/');
};

module.exports = login;

