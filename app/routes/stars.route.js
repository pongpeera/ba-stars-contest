var express = require('express');
var router = express.Router();

var star = require("../controllers/stars.controller");

router.post('/add',star.addStar);
router.get('/',function (req,res,next) {
    if(req.session.authentication !== 'administrator' ) {
        res.redirect('/');
    }
    res.render('stars');
});
router.get('/vote_list',function (req,res,next) {
    res.render('vote_list');
});

router.get('/sum_score',function (req,res,next) {
    res.render('sum_score');
});

router.get('/all/std',star.allStudent);
router.get('/all/:type',star.listStar);
router.post('/del',star.removeStar);
router.post('/vote',star.voteStar);

/*************** ส่วนของรายการโหวต ************************/
router.get('/list/boys/:year',star.strVoteTypeBoy);
router.get('/list/girls/:year',star.strVoteTypeGirl);
router.get('/list/ladyBoys/:year',star.strVoteTypeLadyBoy);
router.get('/list/tomBoys/:year',star.strVoteTypeTomBoy);
/*************** จบส่วนของรายการโหวต ************************/

/*************** ส่วนของการโชว์คะแนน ************************/
router.get('/top/boy',star.strScoreTypeBoy);
router.get('/top/girl',star.strScoreTypeGirl);
router.get('/top/Ladyboy',star.strScoreTypeLadyBoy);
router.get('/top/tomboy',star.strScoreTypeTom);

router.get('/list/fullstrmajor',star.fullStarNameMajor);
/*************** จบส่วนของการโชว์คะแนน ************************/


module.exports = router;
