var express = require('express');
var router = express.Router();

var register = require("../controllers/register.controller");


router.get('/', function(req, res, next) {
    res.render('register.ejs');
});

router.post('/',register.insertRegister);
router.post('/edit',register.GetRegister);
router.post('/update',register.UpdateRegister);
module.exports = router;
