var express = require('express');
var router = express.Router();

var admin = require("../controllers/admin.controller");
router.post('/add' ,admin.addnewAdmin);
router.get('/list' ,admin.adminList);
router.post('/del',admin.adminDel);
router.post('/edit',admin.editPassword);
router.get('/get',admin.adminGet);
router.get('/',function (req,res,next) {
    if(req.session.authentication !== 'administrator' ) {
        res.redirect('/');
    }
    res.render('administrators');
});
/***
 * ถ้าจะเลือกส่งข้อมูลเข้าใน routes admin.route ในโปรแกรม POST ห้ามลืม ใส่ add ต่อท้ายเช่น
 * [ http://localhost:3000/admin/add ] จำไว้ !!!! เอออออออออออออ
 */



module.exports = router;
