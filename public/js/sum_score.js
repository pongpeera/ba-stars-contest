/**
 * Created by The Exiled on 2/8/2559.
 */


Vue.component('first-star-image',{
    props : ['subRoute','starId','starYear','starName','voteCount'],
    template: '<div class="row" >'+
                    '<img :src="subRoute | convertImageURL starId starYear" style="width: 15vw" />'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-md-1">'+
                        '<span v-text="starId"></span>'+
                    '</div>'+
                    '<div class="col-md-9">'+
                        '<span v-text="starName"></span>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<span v-text="voteCount"></span>'+
                '</div>'
});

Vue.component('next-star-image',{
    props : ['subRoute','starId','starYear','starName','voteCount'],
    template: '<div class="row">'+
                    '<div class="col-md-3">' +
                        '<img :src="subRoute | convertImageURL starId starYear" style="width: 5vw" />'+
                    '</div>'+
                    '<div class="col-md-9">'+
                        '<div class="row">'+
                            '<span v-text="starId"></span>'+
                        '</div>'+
                        '<div class="row">'+
                            '<span v-text="starName"></span>'+
                        '</div>'+
                        '<div class="row">'+
                            '<span v-text="voteCount"></span>'+
                        '</div>'+
                    '</div>'+
                '</div>'
});


var sumSocre = new Vue({
    el : '#sumSocre',
    components : {
        'progressbar' : progressbar
    },
    ready : function () {
        this.fetchTopBoy();
        this.fetchTopGirl();
        this.fetchTopLadyBoy();
        this.fetchTopTomBoy();
        this.fetchPieChart();
    },
    data : {
        boys : [],
        girls : [],
        lady_boys : [],
        tom_boys : []
    },
    methods : {
        fetchTopBoy : function () {
            this.$http.get('/../stars/top/boy')
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('boys',response.data);
                    }
                },function (err) {
                    console.log(err)
                });
        },
        fetchTopGirl : function () {
            this.$http.get('/../stars/top/girl')
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('girls',response.data);
                    }
                },function (err) {
                    console.log(err)
                });
        }
        ,fetchTopLadyBoy : function () {
            this.$http.get('/../stars/top/Ladyboy')
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('lady_boys',response.data);
                    }
                },function (err) {
                    console.log(err)
                });
        },
        fetchTopTomBoy : function () {
            this.$http.get('/../stars/top/tomboy')
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('tom_boys',response.data);
                    }
                },function (err) {
                    console.log(err)
                });
        },
        fetchPieChart : function () {
            var data = {
                labels: [
                    "ผู้ที่ลงคะแนน",
                    "ผู้ที่ยังไม่ได้ลงคะแนน"
                ],
                datasets: [
                    {
                        backgroundColor: [
                            "#2FB672",
                            "#DF002F",
                        ],
                        hoverBackgroundColor: [
                            "#2FB672",
                            "#DF002F"
                        ]
                    }]
            };
            this.$http.get('/../stars/all/std')
                .then(function (response) {
                    if(response && response.data) {
                        var result = response.data[0];
                        var percent = (result.TotalScoreVote/result.TotalStudent)*100;
                        data.datasets[0].data = [percent,100-percent];
                        var ctx = document.getElementById("pieChart");
                        var myPieChart = new Chart(ctx,{
                            type: 'pie',
                            data: data
                        });
                    }
                },function (err) {
                    console.log(err)
                });
        },
        fetchBarChart : function () {

        }
    }
});