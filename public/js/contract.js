/**
 * Created by The Exiled on 1/8/2559.
 */
var contract = new Vue({
    el : '#contract',
    components : {
        'alert' : alert
    },
    ready :function () {
        document.title = "ติดต่อเรา";
    },
    data : {
        userEmail : '',
        userSubject : '',
        contactContent : '',
        alertShow : false,
        alertMessage : 'ยังไม่มีการทำรายการ'
    },
    methods : {
        onSubmit : function (e) {
            e.preventDefault();
            var contract_data = {
                email : this.$get(),
                subject : this.$get(),
                contactContent : this.$get()
            };
            
            this.$http.post('/contract/send',contract_data)
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('alertShow',true);
                        this.$set('alertMessage','ส่งข้อความถึงเราเรียบร้อยแล้ว กรุณารอการตอบกลับของเราทางอีเมล');
                    }
                },function (err) {
                    console.log(err);
                });
        }
    }
});