/**
 * Created by JJOMJAMM on 18/7/2559.
 */

var connection = require("../../config/database");
var user = {};
// user.listuser = function (req,res,next) {
//   res.send("ListAllUser"); //ใช้test
// };

/************** List User ******************/

user.listUser = function (req,res,next) {
  var sql_user = "select * from tb_member order by student_id ;";
  /**
   * connection.query(sql_user,function (err,rows,field)
   * ถ้ามันมีข้อมูลอันเดียวใช้อันเดียวก็ได้ ไม่จำเป็นต้องหลายอัน
   * ถ้าต้องการแสดงข้อมูลเป็นแถว ให้ใส่ rows เสมอ
   */
  connection.query(sql_user,function (err,rows,field){
      /**
     * ถ้าต้องการดู rows มากกว่า 1 ให้ใส่ >0
     * ในกรณีแสดงแถวเดียวให้ใส่ ===1 แต่ไม่ใช่คำสั่งนี้
     */
    if (!err) {
      if(rows && rows.length > 0){
        res.json(rows);
      }else{
        res.send({ message : "ไม่มีรายการผู้ใช้นี้"});

      }

    }else{
      res.send({ message : "ผิดพลาด"});
    }
  });
};

/************** Delete User ******************/

user.delUser = function (req,res,next) {
  var email = req.body.email;

  var sql_del1 = 'DELETE FROM tb_member where login_email = \''+email+'\'';
  var sql_del2 = 'DELETE FROM tb_login where email = \''+email+'\'';


  connection.query(sql_del1,function (err,result) {
      if(!err){
        connection.query(sql_del2,function (err,result) {
          if(!err){
            res.send({ message : "ทำการลบข้อมูลสำเร็จเรียบร้อย" });
          }
          else {
            res.send({ message : "เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลได้"});
          }

        })
      }
      else {
      }
  })
};


user.listMajor = function (req,res,next) {
  var sql = 'SELECT * FROM tb_major';
  connection.query(sql,function (err,rows,fields) {
    if (!err) {
      if(rows && rows.length > 0) {
        res.json(rows);
      } else {
        res.send({ message : "ไม่มีรายการสาขาวิชา"});
      }
    } else {
      res.send(err);
    }
  });
};

/** เลือกรหัสนักศึกษา เพื่อนำข้อมูลมาโชว์แถวเดียว**/
user.listOneUser = function (req,res,next) {
  var sql_user = 'SELECT * FROM tb_member where student_id =? ;';

  connection.query(sql_user,function (err,rows,field) {
    if(!err){
      if(rows && rows.length >0){
        res.json(rows);
      }else{
        res.send ({ message : "ไม่มีชื่อ User นี้ในรายการ" });
      }
    }else {
      res.send (err);
    }
  })
};

/** โชว์ว่า นักศึกษารหัสที่ต้องการ ได้โหวตประเภทไหนไปแล้วบ้าง**/

user.checkUserVote = function (req,res,next) {
    var id = req.params.id;
  var sql_user = 'select star_id from tb_member join tb_vote on (tb_member.student_id = tb_vote.student_id)' +
      ' where tb_member.student_id = ?;';

  connection.query(sql_user,[id],function (err,rows,field) {
    if(!err){
      if(rows && rows.length >0){
        res.json(rows);
      }else{
        res.send ({ message : " User นี้ไม่ได้ทำการโหวต" });
      }
    }else {
      res.send (err);
    }
  });
};

/*************** ส่วนใส่ในการโชว์ว่าประเภทนั้นมีใครโหวตบ้าง ************************/
user.useVoteTypeBoy = function (req,res,next) {
  var year = req.params.year;
  var sql_user = "select * from tb_member join tb_vote on (tb_member.student_id = tb_vote.student_id) where tb_vote.star_id like 'B%';";


  connection.query(sql_user,[year],function (err,rows,field) {

    if (!err) {
      if(rows && rows.length > 0){
        res.json(rows);
      }else{
        res.send({ message : "ไม่มีประเภทโหวต เดือน ในนี้"});

      }

    }else{
      res.send(err);
    }
  });

};
user.useVoteTypeGirl = function (req,res,next) {
  var year = req.params.year;
  var sql_user = "select * from tb_member join tb_vote on (tb_member.student_id = tb_vote.student_id) where tb_vote.star_id like 'G%';";


  connection.query(sql_user,[year],function (err,rows,field) {

    if (!err) {
      if(rows && rows.length > 0){
        res.json(rows);
      }else{
        res.send({ message : "ไม่มีประเภทโหวต ดาว ในนี้"});

      }

    }else{
      res.send(err);
    }
  });

};
user.useVoteTypeLadyBoy = function (req,res,next) {
  var year = req.params.year;
  var sql_user = "select * from tb_member join tb_vote on (tb_member.student_id = tb_vote.student_id) where tb_vote.star_id like 'K%';";


  connection.query(sql_user,[year],function (err,rows,field) {

    if (!err) {
      if(rows && rows.length > 0){
        res.json(rows);
      }else{
        res.send({ message : "ไม่มีประเภทโหวต ดาวเทียม ในนี้"});

      }

    }else{
      res.send(err);
    }
  });

};
user.useVoteTypeTomBoy = function (req,res,next) {
  var year = req.params.year;
  var sql_user = "select * from tb_member join tb_vote on (tb_member.student_id = tb_vote.student_id) where tb_vote.star_id like 'T%';";


  connection.query(sql_user,[year],function (err,rows,field) {

    if (!err) {
      if(rows && rows.length > 0){
        res.json(rows);
      }else{
        res.send({ message : "ไม่มีประเภทโหวต เดือนเทียม ในนี้"});

      }

    }else{
      res.send(err);
    }
  });

};


module.exports = user;
