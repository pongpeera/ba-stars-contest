/**
 * Created by The Exiled on 1/8/2559.
 */
// var currentYear = (new Date().getFullYear()+543);
var currentYear = 2560;


var stars = new Vue({
    el : '#stars',
    components : {
        'modal' : modal,
        'alert' : alert
    },
    ready : function () {
        var self = this;
        document.title = "จัดการข้อมูลผู้ใช้ผู้เข้าประกวด";
        var years = [];
        for (var i = 2559 ; i <= currentYear ; i++ ) {
            years.push(i);
        }
        console.log(years)
        self.$set('list_year',years);
        self.fetchStars();

        self.$http.get('user/list/major')
            .then(function (response) {
                if(response && response.data) {
                    self.$set('listMajor',response.data);
                }
            },function (err) {
                console.log(err);
            });
    },
    data : {
        select_year :currentYear,
        list_year : [ currentYear],
        star_type : 'girls',
        addStarButton : false,
        stars : [],
        confirmRemoveStar : false,
        showAlert : false,
        alertMessage : 'ยังไม่มีการทำรายการ',
        removeStarId : undefined,
        api_url : undefined,
        listMajor : [],
        starMajor : 1
    },
    methods : {
        fetchStars : function () {
            var api = '/../stars/list/'+this.$get('star_type')+'/'+this.$get('select_year');
            this.$http.get(api)
                .then(function (response) {
                    if(response && response.data) {
                        this.$set('stars',response.data);
                    }
                },function (err) {
                    console.log();
                });
        },
        deleteStar : function (starId) {
            var data = {
                starId : starId,
                year : this.$get('year')
            };
            this.$http.post('/../stars/del',data)
                .then(function (response) {
                    if (response && response.data) {
                        this.$set('alertMessage',response.data.message);
                        this.$set('showAlert',true);
                        this.$set('confirmRemoveStar',false);
                        this.fetchStars();
                    }
                },function (err) {
                    console.log(err)
                });

        },
        onSubmit : function () {
            var form = this.$els.starForm;
            form.submit();
        }
    }
});