/**
 * Created by The Exiled on 19/7/2559.
 */
var connection = require("../../config/database");
var Users = {};

Users.list = function () {
  connection.connect();
  connection.query('SELECT * FROM mydb.tb_member',function (err,rows,feilds) {
    connection.end();
    if(!err) {
      if(rows && rows > 0) {
        return rows;
      } else {
        return false;
      }
    } else {
      return false;
    }
  });
};

Users.get = function (email) {
  if(!email) {
    return false;
  }
  var sql = 'SELECT * FROM mydb.tb_member member join mydb.tb_login login where login.email = member.login_email and login.email ?';
  connection.connect();
  connection.query(sql,[email],function (err,rows,feilds) {
    connection.end();
    if(!err) {
      if(rows && rows === 1) {
        return rows;
      } else {
        return false
      }
    } else {
      return false;
    }
  });
};

Users.add = function (profile) {
  if(!profile) {
    return false;
  }
  var table = (profile.student_id) ? 'tb_member' : 'tb_login';
  connection.connect();
  connection.query('INSERT INTO '+table+' SET  ?',profile,function (err,result) {
    connection.end();
    if(!err) {
      return true;
    } else {
      return false;
    }
  });
};


Users.edit = function (student_id) {
  if(!profile) {
    return false;
  }
  var table = (profile.student_id) ? 'tb_member' : 'tb_login';
  connection.connect();
  connection.query('UPDATE INTO '+table+' SET  ? WHERE email = '+profile.email,profile,function (err,result) {
    connection.end();
    if(!err) {
      return true;
    } else {
      return false;
    }
  });
};

Users.remove = function (profile) {
  if(!profile) {
    return false;
  }
  var table = (profile.student_id) ? 'tb_member' : 'tb_login';
  var column = (profile.student_id) ? 'tb_member' : 'tb_login';
  connection.connect();
  connection.query('DELETE FROM '+table+' SET  ? WHERE '+column +' = '+profile.email,profile,function (err,result) {
    connection.end();
    if(!err) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports = Users;
